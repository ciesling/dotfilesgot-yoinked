return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	config = function()
		local configs = require("nvim-treesitter.configs")
		configs.setup({
			ensure_installed = {
				"query",
				"vimdoc",
				"vim",
				"lua",
				"c",

				"html",
				"css",
				"scss",
				"javascript",
				"markdown",
				"json",

				"python",
				"bash",

				-- in order for some plugins to work properly
				"jsonc",
				"markdown_inline",
			},
			ignore_install = {},
			modules = {},
			sync_install = false,
			auto_install = true,
			highlight = { enable = true },
			indent = { enable = true },
		})
	end,
}
