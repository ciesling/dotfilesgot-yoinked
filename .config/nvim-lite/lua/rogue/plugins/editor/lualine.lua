return {
	"nvim-lualine/lualine.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {
		options = {
			theme = "auto",
			globalstatus = true,
			disabled_filetypes = { statusline = { "dashboard", "alpha", "starter" } },
		},
		extensions = {
			"neo-tree",
		},
	},
	config = function(plugins, opts)
		require("lualine").setup(opts)
	end,
}
