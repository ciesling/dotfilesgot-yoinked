return { -- For installing LSPs, DAPs, formatters, linters
	"williamboman/mason.nvim",
	dependencies = {
		"williamboman/mason-lspconfig.nvim",
		"WhoIsSethDaniel/mason-tool-installer.nvim",
	},
	config = function()
		-- The one and only
		local mason = require("mason")
		-- For installing LSPs and DAPs
		local mason_lspconfig = require("mason-lspconfig")
		-- For installing formatters, linters ... etc
		local mason_tool_installer = require("mason-tool-installer")

		mason.setup({
			ui = {
				border = "none",
				width = 0.8,
				height = 0.8,
				icons = {
					package_installed = "✓",
					package_pending = "➜",
					package_uninstalled = "✗",
				},
			},
		})

		mason_lspconfig.setup({
			-- LSPs, DAPs
			ensure_installed = {
				-- Web
				"html",
				"cssls",
				"tsserver",
				"jsonls",

				-- Generic
				"lua_ls",
				"bashls",
			},
			automatic_installation = true,
		})

		mason_tool_installer.setup({
			-- Formatters, Linters
			ensure_installed = {
				-- Fomratters
				"stylua",
				"prettier",
        "shfmt",

				-- Linters
				"selene",
				"codespell",
				"eslint",
				"stylelint",

				-- Both
			},
      auto_update = true,
		})
	end,
}
