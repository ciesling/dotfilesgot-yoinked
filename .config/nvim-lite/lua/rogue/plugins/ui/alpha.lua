---@diagnostic disable: need-check-nil

local asciis = {
	["neovim-lite"] = {
		[[                               __                     ]],
		[[  ___     ___    ___   __  __ /\_\    ___ ___         ]],
		[[ / _ `\  / __`\ / __`\/\ \/\ \\/\ \  / __` __`\       ]],
		[[/\ \/\ \/\  __//\ \_\ \ \ \_/ |\ \ \/\ \/\ \/\ \      ]],
		[[\ \_\ \_\ \____\ \____/\ \___/  \ \_\ \_\ \_\ \_\     ]],
		[[ \/_/\/_/\/____/\/___/  \/__/    \/_/\/_/\/_/\/_/ lite]],
	},
	["nvim-lite"] = {
		[[                                               ___              __              ]],
		[[                   __                         /\_ \      __    /\ \__           ]],
		[[  ___     __  __  /\_\     ___ ___            \//\ \    /\_\   \ \ ,_\     __   ]],
		[[/' _ `\  /\ \/\ \ \/\ \  /' __` __`\   ______   \ \ \   \/\ \   \ \ \/   /'__`\ ]],
		[[/\ \/\ \ \ \ \_/ | \ \ \ /\ \/\ \/\ \ /\_____\   \_\ \_  \ \ \   \ \ \_ /\  __/ ]],
		[[\ \_\ \_\ \ \___/   \ \_\\ \_\ \_\ \_\\/_____/   /\____\  \ \_\   \ \__\\ \____\]],
		[[ \/_/\/_/  \/__/     \/_/ \/_/\/_/\/_/           \/____/   \/_/    \/__/ \/____/]],
	},
}

return {
	"goolord/alpha-nvim",
	event = "VimEnter",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
		"nvim-lua/plenary.nvim",
	},
	config = function()
		local alpha = require("alpha")
		local dashboard = require("alpha.themes.dashboard")
		dashboard.section.header.val = asciis["nvim-lite"]
		dashboard.section.buttons.val = {
			dashboard.button("e", "  New file", ":ene <BAR> startinsert <CR>"),

			dashboard.button("f", "󰍉  Find files", "<cmd>lua require('telescope.builtin').fd()<cr>"),

			dashboard.button("r", "  Find recent files", "<cmd>lua require('telescope.builtin').oldfiles()<cr>"),

			dashboard.button("l", "󰒲  Open lazy menu", "<cmd>Lazy<cr>"),

			dashboard.button("m", "󱌣  Open mason menu", "<cmd>Mason<cr>"),

			dashboard.button("h", "󰓙  Run healthcheck", "<cmd>checkhealth<cr>"),

			dashboard.button("c", "󱁻  Go to nvim-lite config directory", ":cd ~/.config/nvim-lite<cr>"),

			dashboard.button("q", "󰅚  Quit Neovim", ":qa<CR>"),
		}
		local handle = io.popen("fortune")
		local fortune = handle:read("*a")
		handle:close()
		dashboard.section.footer.val = fortune

		dashboard.config.opts.noautocmd = true

		vim.cmd([[autocmd User AlphaReady echo 'ready']])

		alpha.setup(dashboard.config)
	end,
}
