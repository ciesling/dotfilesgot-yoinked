return {
	-- For color highlighting
	{
		"NvChad/nvim-colorizer.lua",
		event = { "BufReadPre", "BufNewFile" },
		config = true,
	},
	-- For color picking
	{
		"ziontee113/color-picker.nvim",
		event = { "BufReadPost", "BufNewFile" },
		keys = {
			{ "<C-c>", "<cmd>PickColor<cr>", mode = "n", desc = "Pick color" },
			{ "<C-c>", "<cmd>PickColorInsert<cr>", mode = "i", desc = "Pick color and insert" },
		},
		config = function()
			require("color-picker").setup({
				["icons"] = { "ﱢ", "" },
				["border"] = "single", -- none | single | double | rounded | solid | shadow
				["keymap"] = {
					["U"] = "<Plug>ColorPickerSlider5Decrease",
					["O"] = "<Plug>ColorPickerSlider5Increase",
				},
				["background_highlight_group"] = "Normal",
				["border_highlight_group"] = "FloatBorder",
				["text_highlight_group"] = "Normal",
			})

			-- vim.cmd([[hi FloatBorder guibg=NONE]]) -- if you don't want weird border background colors around the popup.
		end,
	},
}
